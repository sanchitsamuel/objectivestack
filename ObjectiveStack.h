#include <iostream>

class Stack
{
    int data;
    class Stack *next;
    class Stack *rear;

public:
    Stack(int data);
    void printStack();
    void push();
    void pop();
} *top;
